CREATE DATABASE ssd;
use ssd;
CREATE TABLE IF NOT EXISTS `users` (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(30),
  mot_passe  varchar(100) NOT NULL,
  nom varchar(60) NOT NULL,
  prenom varchar(60) NOT NULL,
  email  varchar(100) NOT NULL,
  active BOOLEAN DEFAULT true
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `users` ADD CONSTRAINT unique_email UNIQUE (email);

CREATE TABLE IF NOT EXISTS `deplacements` (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  adresse varchar(255) NOT NULL,
  date_sortie DATETIME NOT NULL,
  heure_sortie DATETIME NOT NULL,
  heure_retour DATETIME NOT NULL,
  annule BOOLEAN DEFAULT false,
  email  varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
