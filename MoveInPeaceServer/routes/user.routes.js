module.exports = app => {
    const user =  require("../_src/users/user.controller.js");
  
    // Create a new Deplacement
    app.post("/authenticate", user.authenticate);
  
    app.post("/user", user.create);

    // Retrieve all Deplacements
    app.get("/user", user.getAll);
  
    // Retrieve a single Deplacement with username
    app.get("/user/:username", user.findOne);
  
    // Update a Deplacement with username
    app.put("/user/:username", user.update);
  
    // Delete a Deplacement with username
    app.delete("/user/:username", user.delete);
  
    // Create a new Deplacement
    app.delete("/user", user.deleteAll);
  };