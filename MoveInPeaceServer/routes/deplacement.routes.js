module.exports = app => {
    const deplacement = require("../_src/planification/deplacement.controller.js");
  
    // Create a new Deplacement
    app.post("/deplacement", deplacement.create);
  
    // Retrieve all Deplacements
    app.get("/deplacement", deplacement.findAll);
  
    // Retrieve a single Deplacement with deplacementId
    app.get("/deplacement/:deplacementId", deplacement.findOne);
  
    // Update a Deplacement with deplacementId
    app.put("/deplacement/:deplacementId", deplacement.update);
  
    // Delete a Deplacement with deplacementId
    app.delete("/deplacement/:deplacementId", deplacement.delete);
  
    // Create a new Deplacement
    app.delete("/deplacement", deplacement.deleteAll);
  };