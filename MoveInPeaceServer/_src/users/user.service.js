﻿const config = require('./config.json');
const jwt = require('jsonwebtoken');
const sql = require("../../_helpers/db.js");
const util = require('util');

// constructor
const User = function (user) {
    this.username = user.email;
    this.mot_passe = user.mot_passe;
    this.nom = user.nom;
    this.prenom = user.prenom;
    this.active = user.active;
    this.email = user.email;
};

User.authenticate = async ({ email, mot_passe }) => {
    let user = await login(email, mot_passe);
    console.log("user ::", JSON.stringify(user))
    if (user) {
        const token = jwt.sign({ sub: user.id }, config.secret);
        const { mot_passe, ...userWithoutPassword } = user;
        return {
            ...userWithoutPassword,
            token
        };
    }
};

function login(email, pass) {
    let qry = `SELECT * FROM users WHERE email = "${email}" and mot_passe = "${pass}"`;
    console.log(qry);
    return new Promise(function (resolve) {
        sql.query(qry, (err, res) => {
            if (err) {
                console.log("error: ", err);
                resolve(null);
            }
            if (res.length) {
                console.log("found user: ", res[0]);
                resolve(res[0]);
            }
            // not found User with the id
            resolve(null);
        });
    });
}




User.getAll = async () => {
    return users.map(u => {
        const { password, ...userWithoutPassword } = u;
        return userWithoutPassword;
    });
};

User.create = (newUser, result) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created user: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};

User.findById = (email, result) => {
    sql.query(`SELECT * FROM users WHERE email = ${email}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found User with the id
        result({ kind: "not_found" }, null);
    });
};


User.updateById = (id, user, result) => {
    sql.query(
        "UPDATE users SET prenom = ?, nom = ? WHERE email = ?",
        [user.email, user.name, user.active, username],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found User with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated user: ", { username: username, ...user });
            result(null, { username: username, ...user });
        }
    );
};

User.remove = (id, result) => {
    sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found User with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted user with id: ", id);
        result(null, res);
    });
};

User.removeAll = result => {
    sql.query("DELETE FROM users", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} users`);
        result(null, res);
    });
};

module.exports = User;