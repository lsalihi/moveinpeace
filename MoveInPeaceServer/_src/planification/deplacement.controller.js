const Deplacement = require("./deplacement.service.js");

// Create and Save a new Deplacement
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a Deplacement
  const deplacement = new Deplacement({
    adresse: req.body.adresse,
    date_sortie: new Date(req.body.dateSortie),
    heure_sortie: new Date(req.body.heureSortie),
    heure_retour: new Date(req.body.heureRetour),
    email: req.body.email
  });

  // Save Deplacement in the database
  Deplacement.create(deplacement, (err, data) => {
    console.log('create');
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Deplacement."
      });
    else res.send(data);
  });
};

// Retrieve all Deplacements from the database.
exports.findAll = (req, res) => {
  console.log('findAll' + JSON.stringify(req.params) + JSON.stringify(req.query));
  Deplacement.getAll({date :req.query.date, adresse : req.query.adresse}, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving deplacements."
      });
    else res.send(data);
  });
};

// Find a single Deplacement with a deplacementId
exports.findOne = (req, res) => {
  console.log('findOne');
  Deplacement.findById(req.params.deplacementId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Deplacement with id ${req.params.deplacementId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Deplacement with id " + req.params.deplacementId
        });
      }
    } else res.send(data);
  });
};

// Update a Deplacement identified by the deplacementId in the request
exports.update = (req, res) => {
  console.log('update');
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  Deplacement.updateById(
    req.params.deplacementId,
    new Deplacement(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Deplacement with id ${req.params.deplacementId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Deplacement with id " + req.params.deplacementId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a Deplacement with the specified deplacementId in the request
exports.delete = (req, res) => {
  console.log('delete');
  Deplacement.remove(req.params.deplacementId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Deplacement with id ${req.params.deplacementId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Deplacement with id " + req.params.deplacementId
        });
      }
    } else res.send({ message: `Deplacement was deleted successfully!` });
  });
};

// Delete all Deplacements from the database.
exports.deleteAll = (req, res) => {
  console.log('deleteAll');
  Deplacement.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all deplacements."
      });
    else res.send({ message: `All Deplacements were deleted successfully!` });
  });
};
