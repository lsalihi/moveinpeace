const sql = require("../../_helpers/db.js");
var dateFormat = require('dateformat');

// constructor
const Deplacement = function (deplacement) {
  this.adresse = deplacement.adresse;
  this.date_sortie = deplacement.date_sortie;
  this.heure_sortie = deplacement.heure_sortie;
  this.heure_retour = deplacement.heure_retour;
  this.annule = deplacement.annule;
  this.email = deplacement.email;
};

Deplacement.create = (newDeplacement, result) => {
  sql.query("INSERT INTO deplacements SET ?", newDeplacement, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created deplacement: ", { id: res.insertId, ...newDeplacement });
    result(null, { id: res.insertId, ...newDeplacement });
  });
};

Deplacement.findById = (deplacementId, result) => {
  sql.query(`SELECT * FROM deplacements WHERE id = ${deplacementId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found deplacement: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Deplacement with the id
    result({ kind: "not_found" }, null);
  });
};

Deplacement.getAll = (filtre, result) => {

  let date = null;
  let adresse = null;
  if (filtre.date && "null" != filtre.date) {
    date = dateFormat(new Date(filtre.date), "yyyy-mm-dd");
    console.log("filtre.date ="+date)
  }
  if (filtre.adresse && "null" != filtre.adresse) {
    adresse = filtre.adresse;
    console.log("filtre.adresse ="+adresse)
  }

  let qry = `SELECT * FROM deplacements`
  if (adresse) {
    qry += ` WHERE adresse like '%${adresse}%'`
  }
  if (date) {
    if (adresse) {
      qry += ` AND DATE_FORMAT(date_sortie, '%Y-%m-%d') = DATE_FORMAT("${date}", '%Y-%m-%d')`
    } else {
      qry += ` WHERE DATE_FORMAT(date_sortie, '%Y-%m-%d') = DATE_FORMAT("${date}", '%Y-%m-%d')`
    }
  }

  console.log(JSON.stringify(filtre) + '|' + qry);
  sql.query(qry, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("deplacements: ", res);
    result(null, res);
  });
};

Deplacement.updateById = (id, deplacement, result) => {
  sql.query(
    "UPDATE deplacements SET adresse = ?, date_sortie = ?, heure_sortie = ?, heure_retour = ?, annule = ? WHERE id = ?",
    [deplacement.email, deplacement.name, deplacement.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Deplacement with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated deplacement: ", { id: id, ...deplacement });
      result(null, { id: id, ...deplacement });
    }
  );
};

Deplacement.remove = (id, result) => {
  sql.query("DELETE FROM deplacements WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Deplacement with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted deplacement with id: ", id);
    result(null, res);
  });
};

Deplacement.removeAll = result => {
  sql.query("DELETE FROM deplacements", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} deplacements`);
    result(null, res);
  });
};

module.exports = Deplacement;
