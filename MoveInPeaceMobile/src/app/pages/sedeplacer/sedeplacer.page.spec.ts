import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeplacerPage } from './sedeplacer.page';

describe('SedeplacerPage', () => {
  let component: SedeplacerPage;
  let fixture: ComponentFixture<SedeplacerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeplacerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeplacerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
