import { Component, OnInit, NgZone } from '@angular/core';
import { PlanificationSortie } from 'src/app/resolvers/planification.model';
import { ToastService } from 'src/app/services/toast.service';
import { SedeplacerService } from 'src/app/services/sedeplacer.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-sedeplacer',
  templateUrl: './sedeplacer.page.html',
  styleUrls: ['./sedeplacer.page.scss'],
})
export class SedeplacerPage implements OnInit {
  public authUser: any;
  address: Object;
  establishmentAddress: Object;
  formattedAddress: string;
  formattedEstablishmentAddress: string;

  planification: PlanificationSortie = {
    adresse: null,
    dateSortie: null,
    heureRetour: null,
    heureSortie: null,
    email: null,
  };
  selectedFiltre: String = "establishment";
  jplus1 = "";
  today: string;

  constructor(public zone: NgZone,
    public toastService: ToastService,
    public sedeplacerService: SedeplacerService,
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.today = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
    });
    if (!this.authUser || !this.authUser.email) {
      this.auth.logout();
    }
    console.log("stor user ==", JSON.stringify(this.authUser));
  }


  getAddress(place: object) {
    this.address = place['formatted_address'];
    this.formattedAddress = place['formatted_address'];
    this.zone.run(() => this.formattedAddress = place['formatted_address']);

    console.log(this.address);
    this.planification.adresse = '' + this.address;
  }

  getEstablishmentAddress(place: object) {
    this.establishmentAddress = place['formatted_address'];
    this.formattedEstablishmentAddress = place['formatted_address'];
    this.zone.run(() => {
      this.formattedEstablishmentAddress = place['formatted_address'];
    });
    console.log(this.formattedEstablishmentAddress);
    this.planification.adresse = '' + this.establishmentAddress;
  }

  valide() {
    console.log(this.planification);
    if (this.planification.adresse == null ||
      this.planification.dateSortie == null ||
      this.planification.heureRetour == null ||
      this.planification.heureSortie == null) {
      this.toastService.presentToast('Données de planification incomplètes');
    } else {
      this.planification.email = this.authUser.email;
      let ok = false;
      this.sedeplacerService.saveDeplacement(this.planification).subscribe(
        (res: any) => {
          alert(this.router.parseUrl)
          //this.router.navigate(['home/feed']);
        },
        (error: any) => {
          this.toastService.presentToast('Erreur réseau.');
        }
      );
      this.planification = {
        adresse: '' + this.address,
        dateSortie: null,
        heureRetour: null,
        heureSortie: null
      };
    }

  }
  radioGroupChange(event) {
    console.log("selectedFiltre ", event.detail);
    this.selectedFiltre = event.detail.value;
  }

  heureOnchange(event) {
    if (this.planification.heureRetour != null &&
      this.planification.heureSortie != null) {
      this.isJplus1();
    }
  }

  isJplus1() {
    console.log(this.planification.heureRetour + '///' + this.planification.heureSortie)
    if (this.planification.heureRetour < this.planification.heureSortie) {
      this.jplus1 = "```````(J+1)"
      console.log(this.jplus1)
    } else {
      this.jplus1 = null;
    }
  }
}
