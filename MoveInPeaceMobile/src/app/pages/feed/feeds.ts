import { PlanificationSortie } from 'src/app/resolvers/planification.model';

export const FEEDS: PlanificationSortie[] = [
    {
        adresse: "8 Rue de Belfort, 33600 Pessac, France",
        dateSortie: new Date(),
        heureRetour: new Date('2020-04-14T13:00:00'),
        heureSortie: new Date('2020-04-14T12:00:00')
    },
    {
        adresse: "8 Rue de Belfort, 33600 Pessac, France",
        dateSortie: new Date(),
        heureRetour: new Date('2020-04-14T13:30:00'),
        heureSortie: new Date('2020-04-14T13:00:00')
    },
    {
        adresse: "8 Rue de Belfort, 33600 Pessac, France",
        dateSortie: new Date(),
        heureRetour: new Date('2020-04-14T17:00:00'),
        heureSortie: new Date('2020-04-14T18:00:00')
    },
    {
        adresse: "8 Rue de Belfort, 33600 Pessac, France",
        dateSortie: new Date(),
        heureRetour: new Date('2020-04-14T08:00:00'),
        heureSortie: new Date('2020-04-14T09:00:00')
    },
    {
        adresse: "8 Rue de Belfort, 33600 Pessac, France",
        dateSortie: new Date(),
        heureRetour: new Date('2020-04-14T10:00:00'),
        heureSortie: new Date('2020-04-14T12:00:00')
    }
];