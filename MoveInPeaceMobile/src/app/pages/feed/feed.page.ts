import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { FeedService } from 'src/app/services/feed.service';
import { AuthService } from './../../services/auth.service';
import { ToastService } from './../../services/toast.service';
import { Chart } from 'chart.js';
import { FEEDS } from './feeds';
import { formatDate } from '@angular/common';
import { SedeplacerService } from 'src/app/services/sedeplacer.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss']
})
export class FeedPage implements OnInit, Onaf {
  public authUser: any;

  address: Object;
  establishmentAddress: Object;
  formattedAddress: string;
  formattedEstablishmentAddress: string;

  @ViewChild('barChart', { static: false }) barChart;
  bars: any;
  colorArray: any;

  today: string;
  dateFiltre: Date = new Date();
  addresseFiltre: String = null;
  dateFiltreheader: Date = null;
  addresseFiltreheader: String = null;

  selectedFiltre: String = "establishment";

  data = [];
  chartData = [];
  hours = ['00h', '1h', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', '11h', '12h',
    '13h', '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h'];

  postData = {
    email: '',
    token: ''
  };
  constructor(public zone: NgZone,
    private auth: AuthService,
    private feedSerive: FeedService,
    private sedeplacerService: SedeplacerService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.today = formatDate(new Date(), 'yyyy-MM-dd', 'en-US'); 
    this.dateFiltre = new Date();
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
    });
    console.log("stor user ==", JSON.stringify(this.authUser));


  }

  feedData() {
    console.log("stor user!! ==", this.authUser);
    this.postData.email = this.authUser.user_id;
    this.postData.token = this.authUser.token;
    if (this.postData.email && this.postData.token) {
      this.feedSerive.feedData(this.postData).subscribe(
        (res: any) => {
          this.feedSerive.changeFeedData(res.feedData);
        },
        (error: any) => {
          this.toastService.presentToast('Erreur réseau.');
        }
      );
    }
  }

  getAddress(place: object) {
    this.address = place['formatted_address'];
    this.formattedAddress = place['formatted_address'];
    this.zone.run(() => this.formattedAddress = place['formatted_address']);

    console.log(this.address);
    this.addresseFiltre = '' +this.address;
  }

  getEstablishmentAddress(place: object) {
    this.establishmentAddress = place['formatted_address'];
    this.formattedEstablishmentAddress = place['formatted_address'];
    this.zone.run(() => {
      this.formattedEstablishmentAddress = place['formatted_address'];
    });
    console.log(this.formattedEstablishmentAddress);
    this.addresseFiltre =  '' + this.establishmentAddress;
  }


  ionViewDidEnter() {
    this.getDataChart(this.hours);

    /** remplir la chart avec les données */
    this.charRemplir();
  }


  getDataChart(hours: string[]) {
    this.dateFiltreheader = this.dateFiltre;
    this.addresseFiltreheader = this.addresseFiltre;
    this.sedeplacerService.getDeplacementData({
      dateFiltre: this.dateFiltre,
      addresseFiltre: this.addresseFiltre

    }).subscribe(res => {
      if (res) {
        this.data = res;
      } else {
        this.data = [];
      }


      let map = new Map();
      console.log(this.data);
      this.data.forEach(d => {
        //get hours interval
        console.log(d.heure_sortie + '|' + d.heure_retour)
        let heureSortie = new Date(d.heure_sortie).getHours();
        let heureRentree = new Date(d.heure_retour).getHours();
        if (heureSortie <= heureRentree) {
          //same day
          while (heureSortie <= heureRentree) {
            if (map.get(this.hourToString(heureSortie)) != null && map.get(this.hourToString(heureSortie)) != undefined) {
              map.set(this.hourToString(heureSortie), map.get(this.hourToString(heureSortie)) + 1)
            } else {
              map.set(this.hourToString(heureSortie), 1)
            }
            heureSortie++;
          }
        } else {
          while (heureSortie <= 23) {
            if (map.get(this.hourToString(heureSortie)) != null && map.get(this.hourToString(heureSortie)) != undefined) {
              map.set(this.hourToString(heureSortie), map.get(this.hourToString(heureSortie)) + 1)
            } else {
              map.set(this.hourToString(heureSortie), 1)
            }
            heureSortie++;
          }
          /*
          J+1
          heureSortie = 0;
          while (heureSortie <= heureRentree) {
            if (map.get(this.hourToString(heureSortie)) != null && map.get(this.hourToString(heureSortie)) != undefined) {
              map.set(this.hourToString(heureSortie), map.get(this.hourToString(heureSortie)) + 1)
            } else {
              map.set(this.hourToString(heureSortie), 1)
            }
            heureSortie++;
          }*/
        }
      });

      console.log('map ==> ', map);
      let result = [];
      hours.forEach(h => {
        let count = map.get(h);
        if (count != null && count != undefined) {
          result.push(count);
        } else {
          result.push(0);
        }
      });
      this.chartData = result;
      console.log(this.chartData);

      /** remplir la chart avec les données */
      this.charRemplir();
    })

  }
  charRemplir() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: this.hours,
        datasets: [{
          label: 'Individus en déplacement',
          data: this.chartData,
          backgroundColor: 'rgb(38, 194, 129)',
          borderColor: 'rgb(38, 194, 129)',
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  hourToString(hour: any) {
    let result = "";
    if (hour <= 9) {
      result = '0' + hour;
    } else {
      result = hour;
    }
    result += "h";
    return result;
  }
  fetchData() {
    console.log('fetchData')
    this.dateFiltreheader = this.dateFiltre;
    this.addresseFiltreheader = this.addresseFiltre;
    this.ionViewDidEnter();
  }

  initData() {
    this.dateFiltre = new Date();
    this.addresseFiltre = null;
    this.selectedFiltre = "establishment";
    this.fetchData();
  }

  radioGroupChange(event) {
    console.log("selectedFiltre ", event.detail);
    this.selectedFiltre = event.detail.value;
  }
}
