import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from './../../config/auth-constants';
import { AuthService } from './../../services/auth.service';
import { StorageService } from './../../services/storage.service';
import { ToastService } from './../../services/toast.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage implements OnInit {
  postData = {
    nom: '',
    prenom: '',
    username: '',
    email: '',
    mot_passe: ''
  };

  constructor(
    private authService: AuthService,
    private toastService: ToastService,
    private storageService: StorageService,
    private router: Router,
    public afDB: AngularFireDatabase
  ) { }

  ngOnInit() { }

  add() {
    this.afDB.list('User/').push({
      pseudo: 'lyazid'
    });
  }

  validateInputs() {
    let username = this.postData.username.trim();
    let mot_passe = this.postData.mot_passe.trim();
    let email = this.postData.email.trim();
    //let nom = this.postData.nom.trim();
    return (
      //this.postData.nom &&
      this.postData.username &&
      this.postData.mot_passe &&
      this.postData.email &&
      //nom.length > 0 &&
      email.length > 0 &&
      username.length > 0 &&
      email.length > 0 &&
      mot_passe.length > 0
    );
  }

  signupAction() {
    if (this.validateInputs()) {
      this.authService.signup(this.postData).subscribe(
        (res: any) => {
          if (res) {
            // Storing the User data.
            alert("reponse inscription " +JSON.stringify(res));
            this.storageService
              .store(AuthConstants.AUTH, res)
              .then(res => {
                this.router.navigate(['home']);
              });
          } else {
            this.toastService.presentToast(
              'Données déja existantes, merci de saisir de nouveaux details.'
            );
          }
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    } else {
      this.toastService.presentToast(
        'Veuillez saisir les données du nom, email, login or mot de passe.'
      );
    }
  }
}
