import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from '../../config/auth-constants';
import { AuthService } from './../../services/auth.service';
import { StorageService } from './../../services/storage.service';
import { ToastService } from './../../services/toast.service';


import { Platform } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { Facebook } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  // Sign in using a popup.
  providerFb: firebase.auth.FacebookAuthProvider;
  postData = {
    email: '',
    mot_passe: ''
  };

  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private toastService: ToastService,
    public afDB: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    private fb: Facebook,
    public platform: Platform
  ) {

  }

  ngOnInit() {
    this.providerFb = new firebase.auth.FacebookAuthProvider();
  }

  facebookLogin() {
    if (this.platform.is('cordova')) {
      console.log('PLateforme cordova');
      this.facebookCordova();
    } else {
      console.log('PLateforme Web');
      this.facebookWeb();
    }
  }

  facebookCordova() {
    this.fb.login(['email']).then((response) => {
      const facebookCredential = firebase.auth.FacebookAuthProvider
        .credential(response.authResponse.accessToken);
      firebase.auth().signInWithCredential(facebookCredential)
        .then((success) => {
          //console.log('Info Facebook: ' + JSON.stringify(success));
          this.afDB.object('users/' + success.user.uid).set({
            displayName: success.user.displayName,
            email: success.user.email,
            photoURL: success.user.photoURL
          });

          this.storageService
            .store(AuthConstants.AUTH, {
              username: success.user.displayName,
              email: success.user.email,
            })
            .then(res => {
              this.router.navigate(['home']);
            });

        }).catch((error) => {
          console.log('Erreur: ' + JSON.stringify(error));
        });
    }).catch((error) => { console.log(error); });
  }

  facebookWeb() {
    this.afAuth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((success) => {
        //console.log('Info Facebook: ' + JSON.stringify(success));
        this.afDB.object('users/' + success.user.uid).set({
          displayName: success.user.displayName,
          email: success.user.email,
          photoURL: success.user.photoURL
        });
        this.storageService
        .store(AuthConstants.AUTH, {
          username: success.user.displayName,
          email: success.user.email,
        })
        .then(res => {
          this.router.navigate(['home']);
        });
      }).catch((error) => {
        console.log('Erreur: ' + JSON.stringify(error));
      });
  }

  validateInputs() {
    console.log(this.postData);
    let email = this.postData.email.trim();
    let mot_passe = this.postData.mot_passe.trim();
    return (
      this.postData.email &&
      this.postData.mot_passe &&
      email.length > 0 &&
      mot_passe.length > 0
    );
  }

  loginAction() {
    if (this.validateInputs()) {
      this.authService.login(this.postData).subscribe(
        (res: any) => {
          if (res) {
            console.log(JSON.stringify(res))
            // Storing the User data.
            this.storageService
              .store(AuthConstants.AUTH, res)
              .then(res => {
                this.router.navigate(['home']);
              });
          } else {
            this.toastService.presentToast('Login ou mot de passe incorect.');
          }
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    } else {
      this.toastService.presentToast(
        'Veuillez enter votre login ou le mot passe.'
      );
    }
  }
}
