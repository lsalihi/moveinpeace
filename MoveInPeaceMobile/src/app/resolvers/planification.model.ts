export class PlanificationSortie { 
  adresse : String;
  dateSortie: Date;
  heureSortie : Date;
  heureRetour : Date;
  email?:string
}
