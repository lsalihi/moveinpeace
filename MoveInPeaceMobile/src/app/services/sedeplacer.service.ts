import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SedeplacerService {
  feedData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService, private http: HttpClient) { }

  saveDeplacement(postData: any): Observable<any> {
    return this.httpService.post('deplacement', postData);
  }

  getDeplacementData(params: any): Observable<any[]> {
    const url = environment.apiUrl + 'deplacement';
    var parm = '';
    if (params.dateFiltre != undefined) {
      parm += '?date=' + formatDate(new Date(params.dateFiltre), 'yyyy-MM-dd', 'en-US');
    }else{
      parm += '?date=' + formatDate(new Date(), 'yyyy-MM-dd', 'en-US'); 
    }
    if(params.addresseFiltre != undefined){
      parm +='&adresse=' + params.addresseFiltre;
    }
    return this.http.get<any[]>(url +  parm);
  }

}
