import { TestBed } from '@angular/core/testing';

import { SedeplacerService } from './sedeplacer.service';

describe('SedeplacerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SedeplacerService = TestBed.get(SedeplacerService);
    expect(service).toBeTruthy();
  });
});
